<img src="https://webassets.telerikacademy.com/images/default-source/logos/telerik-academy.svg)" alt="logo" width="300px" style="margin-top: 20px;"/>

## HTML - Tasks

Create the structure of a TODO app. This app will be extended later on in the course with features, such as **add, remove and mark them as done/not done todos**. Use CSS to add styling of your choice.

1.  Each todo has:

    - Id
    - Name
    - Due date
    - IsDone

1.  Use CSS to make the todo app better looking and feel free to express your creativity.

    _Example:_

    !['todo-app'](./imgs/example-solution.png)
