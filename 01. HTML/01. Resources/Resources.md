<img src="https://webassets.telerikacademy.com/images/default-source/logos/telerik-academy.svg)" alt="logo" width="300px" style="margin-top: 20px;"/>

## Additional Resources - Students

- [HTML Elements](https://www.w3schools.com/tags/default.asp)
- [HTML Attributes](https://www.w3schools.com/tags/ref_attributes.asp)
- [HTML Tables](https://www.w3schools.com/html/html_tables.asp)
- [HTML Forms](https://www.w3schools.com/html/html_forms.asp)
