const data = {
  names: ['Jon', 'Sansa', 'Arya', 'Brandon'],
  users: [
    { name: 'Jon', state: 'reborn' },
    { name: 'Sansa', state: 'alive' },
    { name: 'Arya', state: 'nameless' },
    { name: 'Brandon', state: 'three-eyed crow' },
  ],
  families: {
    Jon: 'Targaryen',
    Sansa: 'Stark',
    Arya: 'Stark',
    Brandon: 'Stark',
  },
};

const requestAsPromise = (key) => {
  console.log(`${key} processing...`);

  return new Promise((resolve, reject) => {
    setTimeout(() => {
      resolve(data[key]);
    }, 2000);
  })
};

const resolveData = async () => {

  try {
    const names = await requestAsPromise('names');
    names.forEach((name) => console.log(name));
    const users = await requestAsPromise('users');
    users.forEach((user) => console.log(user));
    const families = await requestAsPromise('families');
    console.log(families);
  } catch (error) {
    console.log(error.message);
  }

  return 'done';
};

resolveData();
