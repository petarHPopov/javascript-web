const data = {
  names: ['Jon', 'Sansa', 'Arya', 'Brandon'],
  users: [
    { name: 'Jon', state: 'reborn' },
    { name: 'Sansa', state: 'alive' },
    { name: 'Arya', state: 'nameless' },
    { name: 'Brandon', state: 'three-eyed crow' },
  ],
  families: {
    Jon: 'Targaryen',
    Sansa: 'Stark',
    Arya: 'Stark',
    Brandon: 'Stark',
  },
};

const getData = (key, cb, errorCB) => {
  console.log(`${key} processing...`);
  
  setTimeout(() => {
    try {
      // throw new Error('Oppsie!');
      cb(data[key]); // data.names (names)
    } catch (error) {
      errorCB(error);
    }
  }, 2000);
}

getData('names', (names) => {
  names.forEach((name) => console.log(name));
  getData('users', (users) => {

    users.forEach((user) => console.log(user));

    getData('families', (families) => {

      console.log(families);

    }, (error3) => {
      console.log(error3.message);
    });
  }, (error2) => {
    console.log(error2.message);
  });
}, (error) => {
  console.log(error.message);
});
