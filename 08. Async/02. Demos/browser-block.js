// Open new browser and paste this code in the console
// Try to click on any button. It is not responsive
// This is what will happen every time you make a call to the server if JavaScript couldn't handle async operations
let n = 0;
while (n < 100000) {
  console.log(n++);
}
