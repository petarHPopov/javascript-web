<img src="https://webassets.telerikacademy.com/images/default-source/logos/telerik-academy.svg)" alt="logo" width="300px" style="margin-top: 20px;"/>

# Additional Resources - Students

- [Asynchronous Programming Fundamentals](https://eloquentjavascript.net/11_async.html)
- [JavaScript Callbacks](https://flaviocopes.com/javascript-callbacks/)
- [JavaScript Promises](https://flaviocopes.com/javascript-promises/)
- [Javascript async-await](https://flaviocopes.com/javascript-async-await/)
- [JavaScript Promises Basics](https://medium.com/quick-code/javascript-promises-in-twenty-minutes-3aac5b65b887)
- [Loupe](http://latentflip.com/loupe/)
- [Jake Archibald: In The Loop - JSConf.Asia](https://www.youtube.com/watch?v=cCOL7MC4Pl0)
