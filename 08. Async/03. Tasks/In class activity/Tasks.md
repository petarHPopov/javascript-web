<img src="https://webassets.telerikacademy.com/images/default-source/logos/telerik-academy.svg" alt="logo" width="300px" style="margin-top: 20px;"/>

## Async - Tasks

You have created a **Todo app** which could **add, remove and mark todos as done/not done** 

!['todo-app'](./imgs/todo-app-animation.gif)

1. First run the server from the folder ```server```

    - Open the terminal and run ```npm install```
    - Then run ```npm start```
    - Now on url ```http://localhost:3000``` a server is serving files

1. Server specification

    - GET ```http://localhost:3000/todos```
        - Return all todos
    - POST ```http://localhost:3000/todos```
      - Body: {name: 'Some name', due: '2019-05-30'}
      - Add a new todo
    - DELETE ```http://localhost:3000/todos/:id```
      - Delete the todo with the specified id
    - PUT ```http://localhost:3000/todos/:id```
      - Body: {name: 'Some name', due: '2019-05-30', isDone: false}
      - Edit the todo with the specified id
      - You could send only part of the data in the body

1. Convert all HTTP calls from Promise based to ```async/await```

    this

    ```js
    todoService.createTodo($text.val(), $date.val())
      .then(response => {
        $text.val();
        $date.val();

        populateTodos(CONTAINER);

        return response.json();
      })
      .then(result => alert(result.msg))
      .catch(error => alert(error.message));
    ```

    becomes this

    ```js
    try {
      const response = await todoService.createTodo($text.val(), $date.val());
      $text.val();
      $date.val();

      await populateTodos(CONTAINER);
      const result = await response.json();
      alert(result.msg);
    } catch(e) {
      alert(e.message);
    }
    ```

1. Now add ```edit``` functionality

    Add `<button edit-id="${todo.id}">Edit</button>` to the todo's template and use the following template for when the app is in edit mode.

    ```JS
    const editTodoView = (todo, container) => {
      const $div = $(container);
      $div.html(`
      <div class="todo-single">
        <input id="edit-todo-name" type="text" value="${todo.name}" />
        <input id="edit-todo-date" type="date" value="${todo.due}"/>
        <button save-id="${todo.id}">Save</button>
      </div>
      `);
    };
    ```

    - Attach two events. One on the ```Edit``` button and one on ```Save``` button

    !['todo-app'](./imgs/todo-app-edit-animation.gif)
