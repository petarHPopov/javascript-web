const express = require('express');
const server = express();
const fs = require('fs');
const bodyParser = require('body-parser');
const moment = require('moment');
const cors = require('cors');

const port = 3000;
const pathTodos = './todos.json';

server.use(cors());
server.use(bodyParser.json());

const getTodos = async (path) => new Promise((resolve, reject) => {
  fs.readFile(path, 'utf8', (err, data) => {
    if (err) {
      reject(err.message);
    }

    resolve(data);
  });
});

const saveTodos = (todos, path) => new Promise((resolve, reject) => {
  fs.writeFile(path, JSON.stringify(todos), (err) => {
    if (err) {
      reject(err.message);
    }

    resolve(todos);
  });
});

server.get('/', async (req, res) => {
  res.redirect('/todos');
});

server.get('/todos', async (req, res) => {
  const todos = JSON.parse(await getTodos(pathTodos)).filter((todo) => !todo.isDeleted);

  res.send(todos);
});

server.post('/todos', async (req, res) => {
  const todos = JSON.parse(await getTodos(pathTodos));
  let todoToAdd = {};
  todoToAdd.id = todos.length;

  let { name, due } = req.body;

  if (name === undefined) {
    return res.send('No name');
  }

  if (!moment(due, ["YYYY-MM-DD"]).isValid()) {
    return res.send('Invalid date!');
  }

  todoToAdd = { ...todoToAdd, name, due, isDone: false, isDeleted: false };

  todos.push(todoToAdd);

  await saveTodos(todos, pathTodos);

  res.send({msg: 'Todo Added!'});
});


server.delete('/todos/:id', async (req, res) => {
  const id = +req.params.id;

  let todos = JSON.parse(await getTodos(pathTodos));

  const todoFound = todos.find(todo => todo.id === id);

  if (todoFound === undefined || todoFound.isDeleted === true) {
    return res.send('No such todo found');
  }

  todos = todos.map((todo) => {
    if (todo.id === id) {
      todo.isDeleted = true;
    }

    return todo;
  });

  await saveTodos(todos, pathTodos);

  res.send({msg: 'Todo Deleted!'});
});


server.put('/todos/:id', async (req, res) => {
  const id = +req.params.id;

  let todos = JSON.parse(await getTodos(pathTodos));
  const todoUpdate = req.body;

  const todoFound = todos.find(todo => todo.id === id);

  if (todoFound === undefined || todoFound.isDeleted === true) {
    return res.send('No such todo found');
  }

  if (todoUpdate) {
    todos = todos.map((todo) => {
      if (todo.id === id) {
        todo.name = todoUpdate.name !== undefined ? todoUpdate.name : todo.name;
        todo.due = todoUpdate.due !== undefined ? todoUpdate.due : todo.due;
        todo.isDone = todoUpdate.isDone !== undefined ? todoUpdate.isDone : todo.isDone;
      }

      return todo;
    });

    await saveTodos(todos, pathTodos);
  }

  res.send({msg: 'Todo Updated!'});
});

server.listen(port, () => {
  console.log(`Listening on: http://localhost:${port}!`);
});
