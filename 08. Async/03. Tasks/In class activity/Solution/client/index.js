import { todoDelete, todoToggle, todoAdd, populateTodos, todoEdit, todoSaveChanges } from './views.js';
import { deleteTodo, addTodo, toggleTodo, editTodo, saveTodoChanges } from './events.js';
import { CONTAINER } from './common.js'


// document.onready
$(async () => {

  // top-level try-catch
  try {
    // Initial populate
  await populateTodos(CONTAINER);

  // Attach events here
  addTodo(todoAdd);

  editTodo(todoEdit);

  saveTodoChanges(todoSaveChanges);
  
  toggleTodo(todoToggle);
  
  deleteTodo(todoDelete);
  } catch (e) {
    alert(e.message)
  }

});
