const CONTAINER = '#todos';

const BASE_URL = `http://localhost:3000`;

export {
  CONTAINER,
  BASE_URL,
}