import { CONTAINER } from './common.js';
import todoService from './todo-service.js';

// Internal for the module
const todoView = (todo, container) => {
  const $div = $(container);
  $div.append(`
  <div>
    <span class="${todo.isDone ? 'todo-done' : ''}">${todo.name} - ${moment(todo.due).format('MMMM Do YYYY')}</span>
    <input data-check-id="${todo.id}" type="checkbox" ${todo.isDone ? 'checked' : ''}/>
    <button data-id="${todo.id}">X</button>
    <button data-edit-id="${todo.id}">Edit</button>
  </div>
  `);
};

const editTodoView = (todo, container) => {
  const $div = $(container);
  $div.html(`
  <div class="todo-single">
    <input id="edit-todo-name" type="text" value="${todo.name}" />
    <input id="edit-todo-date" type="date" value="${todo.due}"/>
    <button save-id="${todo.id}">Save</button>
  </div>
  `);
};

export const populateEditTodos = async (container) => {
  const $div = $(container);
  $div.empty();
  try {
    const data = await todoService.getTodos()

    const result = await data.json();

    result.forEach(todo => editTodoView(todo, container));

  } catch (error) {
    console.log(error.message)
  }
};



export const editTodos = async (ev) => {

  const $name = $('#todo-text')
  const $die = $('#todo-date')

  const todoId = +$(ev.target).attr('data-edit-id');
  
  try {

    await todoService.editTodo(todoId,$name.val(),$die.val());


    await populateEditTodos(CONTAINER);
  } catch (error) {
    console.log(error.message);
  };
};


export const populateTodos = async (container) => {
  const $div = $(container);
  $div.empty();
  try {
    const data = await todoService.getTodos()

    const result = await data.json();

    result.forEach(todo => todoView(todo, container));

  } catch (error) {
    console.log(error.message)
  }
};

export const todoDelete = async (ev) => {

  const todoId = +$(ev.target).attr('data-id');

  try {

    await todoService.deleteTodo(todoId);
    await populateTodos(CONTAINER);

  } catch (error) {
    console.log(error.message);
  };
};

export const todoToggle = async (ev) => {

  const todoId = +$(ev.target).attr('data-check-id');
  const checked = $(ev.target).attr('checked') ? false : true;

  try {

    await todoService.updateToggleTodo(todoId, checked);
    await populateTodos(CONTAINER);
  } catch (error) {
    console.log(error.message);
  };
};


export const todoAdd = async (ev) => {
  const $text = $('#todo-text')
  const $date = $('#todo-date')

  if (!$text.val()) {
    alert('Invalid todo text!');
    return;
  }
  if (!$date.val()) {
    alert('Invalid date!');
    return;
  }
  try {
    await todoService.createTodo($text.val(), $date.val())
    await populateTodos(CONTAINER);
     $text.val('');
     $date.val('');
  } catch (error) {
    console.log(error.message)
  }

};


