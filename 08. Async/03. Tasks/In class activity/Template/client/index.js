import { todoDelete, todoToggle, todoAdd, populateTodos ,populateEditTodos  } from './views.js';
import { deleteTodo, addTodo, toggleTodo,editedTodo} from './events.js';
import { CONTAINER } from './common.js'


// document.onready
$(() => {

  // Initial populate
  populateTodos(CONTAINER);

  // Attach events here
  addTodo(todoAdd);
  
  editedTodo(populateEditTodos);

  toggleTodo(todoToggle);
  
  deleteTodo(todoDelete);

});
