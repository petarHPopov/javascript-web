<img src="https://webassets.telerikacademy.com/images/default-source/logos/telerik-academy.svg)" alt="logo" width="300px" style="margin-top: 20px;"/>

## Additional Resources - Students

- [CSS References](https://www.w3schools.com/cssref/default.asp)
- [CSS Selectors](https://www.w3schools.com/cssref/css_selectors.asp)
- [CSS Units](https://www.w3schools.com/cssref/css_units.asp)
- [CSS Grid](https://learncssgrid.com/)
- [CSS Grid Guide](https://css-tricks.com/snippets/css/complete-guide-grid/)
- [CSS Grid Garden](https://cssgridgarden.com/)
