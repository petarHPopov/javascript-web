(() => {
  const $input = $('#search-term');
  const $resultDiv = $('#result');
  const $pageLimit = $('#page-limit');

  $(document).on('click', '#search-btn', () => {
    $resultDiv.empty();
    const searchTerm = $input.val();
    const pageLimit = $pageLimit.val() === undefined ? 10 : +$pageLimit.val();

    if (searchTerm) {
      fetch(`https://en.wikipedia.org/w/api.php?action=query&srlimit=${pageLimit}&list=search&srsearch=${searchTerm}&utf8=&format=json&origin=*`)
        .then((res) => res.json())
        .then((res) => {
          res.query.search.forEach((result) => {
            $resultDiv.append(`<a href="https://en.wikipedia.org/?curid=${result.pageid}">${result.title}</a><br>`);
            $input.val('');
          });
        });
    }
    else {
      alert('Provide a search term!');
    }
  })
})();
