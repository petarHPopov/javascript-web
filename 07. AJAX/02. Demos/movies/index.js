(() => {
  const $input = $('#search-term');
  const $resultDiv = $('#result');

  $(document).on('click', '#search-btn', () => {
    $resultDiv.empty();
    const searchTitle = $input.val();

    fetch(`https://jsonmock.hackerrank.com/api/movies/search/?Title=${searchTitle}`)
      .then((res) => res.json())
      .then((res) => {
        res.data.forEach((movie) => {
          $resultDiv.append(`<a href="https://www.imdb.com/title/${movie.imdbID}">${movie.Title}</a><br>`);
        });

        $input.val('');
      });
  })
})();
