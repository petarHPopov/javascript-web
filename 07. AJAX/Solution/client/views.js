import { CONTAINER } from './common.js';
import todoService from './todo-service.js';

// Internal for the module
const todoView = (todo, container) => {
  const $div = $(container);
  $div.append(`
  <div>
    <span class="${todo.isDone ? 'todo-done' : ''}">${todo.name} - ${moment(todo.due).format('MMMM Do YYYY')}</span>
    <input data-check-id="${todo.id}" type="checkbox" ${todo.isDone ? 'checked' : ''}/>
    <button data-id="${todo.id}">X</button>
  </div>
  `);
};

export const populateTodos = (container) => {
  const $div = $(container);

  todoService.getTodos()
    .then(response => response.json())
    .then(todos => {
      $div.empty();
      todos.forEach((todo) => todoView(todo, container));
    });
};

export const todoDelete = (ev) => {
  const todoId = +$(ev.target).attr('data-id');

  todoService.deleteTodo(todoId)
    .then(response => populateTodos(CONTAINER))
    .catch(error => alert(error.message));
  ;
};

export const todoToggle = (ev) => {
  const todoId = +$(ev.target).attr('data-check-id');
  const checked = $(ev.target).attr('checked') ? false : true;
  
  todoService.updateToggleTodo(todoId, checked)
    .then(response => {
      populateTodos(CONTAINER);

      return response.json();
    })
    .then(result => alert(result.msg))
    .catch(error => alert(error.message));
};

export const todoAdd = (ev) => {
  const $text = $('#todo-text')
  const $date = $('#todo-date')

  if (!$text.val()) {
    alert('Invalid todo text!');
    return;
  }
  if (!$date.val()) {
    alert('Invalid date!');
    return;
  }

  todoService.createTodo($text.val(), $date.val())
    .then(response => {
      $text.val();
      $date.val();

      populateTodos(CONTAINER);

      return response.json();
    })
    .then(result => alert(result.msg))
    .catch(error => alert(error.message));
};

