<img src="https://webassets.telerikacademy.com/images/default-source/logos/telerik-academy.svg)" alt="logo" width="300px" style="margin-top: 20px;"/>

## AJAX - Tasks

You have created a **Todo app** which could **add, remove and mark them as done/not done todos**. Extend it by adding ```fetch``` from the server 

!['todo-app'](./imgs/todo-app-animation.gif)

1. First run the server from the folder ```server```

    - Open the terminal and run ```npm install```
    - Then run ```npm start```
    - Now on url ```http://localhost:3000``` a server is serving files

1. Server specification

    - GET ```http://localhost:3000/todos ```
        - Return all todos
    - POST ```http://localhost:3000/todos ```
      - Body: {name: 'Some name', due: '2019-05-30'}
      - Add a new todo
    - DELETE ```http://localhost:3000/todos/:id```
      - Delete the todo with the specified id
    - PUT ```http://localhost:3000/todos/:id```
      - Body: {name: 'Some name', due: '2019-05-30', isDone: false}
      - Edit the todo with the specified id
      - You could send only part of the data in the body

1. For HTTP calls you must use ```fetch```

1. The todos are in the format

    ```js
    let todos = [
      { id: 0, name: 'Buy milk', due: '2019-05-30', isDone: true, isDeleted: false },
    ];
    ```

1. You must use ```jQuery``` and you could use a library called ```momentjs``` for formatting the date

    ```js
    // Example
    const date = moment('2019-05-10')
    ```

1. Finish all the functions by using ```fetch```

    - This will add a todo to the server

    ```js
    fetch(`http://localhost:3000/todos`, {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json'
        },
        body: JSON.stringify({ name: 'Some text', due: '2019-08-21' })
      }).then((res) => {
        // What you should do here?
        console.log(res);
      }).catch((err) => alert(err.message));
    ```

    - Every operation must be in ```then``` method, because otherwise the todo won't be added before you do something
