import { todos } from './database.js';

const CONTAINER = '#todos';

const statics = {
  id: todos.length,
};

export { statics, CONTAINER }
