import { CONTAINER } from './common.js';

// Internal for the module
const todoView = (todo, container) => {
  const $div = $(container);
  $div.append(`
  <div>
    <span>${todo.name} - ${moment(todo.due).format('MMMM Do YYYY')}</span>
    <input data-check-id="${todo.id}" type="checkbox" ${todo.isDone ? 'checked' : ''}/>
    <button data-id="${todo.id}">X</button>
  </div>
  `);
};

export const populateTodos = (container) => {
  const $div = $(container);
  $div.empty();

  const url = 'http://localhost:3000/todos';

  fetch(url)
    .then(res => res.json())
    .then(res => {
      res.forEach((todo) => todoView(todo, container))
    })
    .catch((err) => alert(err.message));
};

export const todoDelete = (ev) => {
  const todoId = +$(ev.target).attr('data-id');

  const url = `http://localhost:3000/todos/${todoId}`

  fetch(url, {
    method: 'DELETE',
  })
    .then(res => res.json())
    .then(_ => populateTodos(CONTAINER))
    .catch((err) => alert(err.message));
};

export const todoToggle = (ev) => {
  const todoId = +$(ev.target).attr('data-check-id');

  const isChecked = ev.target.checked;

  console.log(isChecked)

  const url = `http://localhost:3000/todos/${todoId}`

  fetch(url, {
    method: 'PUT',
    body: JSON.stringify({
      isDone: isChecked
    }),
    headers: {
      'Content-Type': 'application/json'
    }
  })
    .then(res => res.json())
    .then(_ => populateTodos(CONTAINER))
    .catch(err => console.log(err.message))
};

export const todoAdd = (ev) => {
  const $text = $('#todo-text')
  const $date = $('#todo-date')

  fetch('http://localhost:3000/todos',
    {
      method: 'POST',
      body: JSON.stringify({
        name: $text.val(),
        due: $date.val()
      }),
      headers: {
        'Content-Type': 'application/json'
      }
    })
    .then(res => res.json())
    .then(_ => populateTodos(CONTAINER))
    .catch((err) => alert(err.message));

  $text.val('');
  $date.val('');

};


