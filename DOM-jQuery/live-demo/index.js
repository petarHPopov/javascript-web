$(() => {
  $(`<h1>`)
    .html('Hello!')
    .attr('id', 'header')
    .on('mouseover', event => $(event.target).css('color', 'red'))
    .on('mouseleave', event => $(event.target).css('color', 'green'))
    .appendTo(document.body);

  // bubbling and capturing
  document
    .getElementsByTagName('ul')[0]
    .addEventListener('click', () => console.log('list'), { capture: true }); // flip the flag and see the change
  document
    .getElementsByTagName('li')[0]
    .addEventListener('click', () => console.log('list item'));
});
