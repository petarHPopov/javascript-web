<img src="https://webassets.telerikacademy.com/images/default-source/logos/telerik-academy.svg)" alt="logo" width="300px" style="margin-top: 20px;"/>

## jQuery - Tasks

Create a **Todo app** which could **add, remove and mark them as done/not done todos**. It shouldn't be pretty at all.

!['todo-app'](./imgs/todo-app-animation.gif)

1. The todos are in the format

   ```js
   let todos = [
     { id: 0, name: 'Buy milk', due: new Date(2019, 05, 30), isDone: true },
     { id: 1, name: 'Buy beer', due: new Date(2019, 06, 03), isDone: false },
     {
       id: 2,
       name: 'Read "The Hitchhiker\'s Guide to the Galaxy" book',
       due: new Date(2019, 06, 14),
       isDone: false
     }
   ];
   ```

1. You must use `jQuery` and you could use a library called `momentjs` for formatting the date

   ```js
   // Example
   const date = moment('2019-05-10');
   ```

1. Create a function `populateTodos(todos, container)` which accepts `todos array and a container descriptor ('#todos')` in which to be rendered

1. For each todo in the array append it in the container with `jQuery`

   - Template to use

     ```html
     `
     <div>
       <input data-check-id="${todo.id}" type="checkbox" ${todo.isDone ?
       'checked' : ''}/>
       <span>${todo.name} - ${moment(todo.due).format('MMMM Do YYYY')}</span>
       <button data-id="${todo.id}">X</button>
     </div>
     `
     ```

   - This uses string interpolation (`${val}`) to inject the values
   - There are some custom attributes which allow you to delete the todo with the corresponding id when you click on them

1. Now it is time to attach the events

   - Add todo - click on `Add` button
   - Remove todo - click on `X` button
   - Mark todo done/not done - click checkbox

1. Put all the event listeners in an IIFE function. This way they will be attached once your JavaScript file is loaded

1. Use dynamic events

   - Select element by attribute [https://api.jquery.com/has-attribute-selector/](https://api.jquery.com/has-attribute-selector/)

   ```js
   // This is the remove event
   $(document).on('click', '[data-id]', ev => {
     const target = $(ev.target);

     // Filter the todos and pass them below. Use the data-id attribute. It is a string by default so you must parse it
     //+$(ev.target).attr('data-id')

     populateTodos(todos, '#todos');
   });
   ```

1. Every time a todo is `added/removed/checked` you must change the todos array and use the `populateTodos()` function

1. Use `statics` object to increment add the id every time you add new todo and attach the id as id of the added todo

   - Steps
     - The todos are 3 (id:0, id:1, id:2)
     - You remove the todo with id = 2
     - Two todos are left with ids (id: 0, id: 1)
     - You add new todo and its id is not 2 but 3, because you never reorder the ids, just increment it

1. Clear the inputs once you add the todo

1. Check if the date is valid and `alert` a message if it is not
