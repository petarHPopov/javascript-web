import { deleteTodo, addTodo, toggleTodo } from './main.js'
import { removTodo , toggle , populateTodos , todoAdd} from './views.js'
import {todos } from './database.js';
import { CONTAINER } from './common.js'


// document.onready
$(() => {
  // Initial populate
  populateTodos(todos, '#todos');
// Attach events here
  addTodo(todoAdd);

  // Remove event
  deleteTodo(removTodo);
 
  // Check event
  toggleTodo(toggle);
})();