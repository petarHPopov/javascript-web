const deleteTodo = (callback) => $(document).on('click', '[data-id]', callback);

const addTodo = (callback) => $(document).on('click', '#btn-add', callback);

const toggleTodo = (callback) => $(document).on('click', '[data-check-id]', callback);

export { deleteTodo, addTodo, toggleTodo }
