import { todos } from './database.js';
import { statics, CONTAINER } from './common.js';

const todoView = (todo, container) => {
    const $div = $(container);
    $div.append(`
    <div class="todo-single">
      <input data-check-id="${todo.id}" type="checkbox" ${
        todo.isDone ? 'checked' : ''
        }/>
      <span>${todo.name} - ${moment(todo.due).format('MMMM Do YYYY')}</span>
      <button data-id="${todo.id}">X</button>
    </div>
    `);
};
export const populateTodos = (todos, container) => {
    const $div = $(container);
    $div.empty();
    todos.forEach((todo) => {
        todoView(todo, container)
    });
   
};
export const removTodo = (ev) => {

    const todoId = +$(ev.target).attr('data-id');
    const foundTodoIndex = todos.findIndex(todo => todo.id === todoId);
  
    if (foundTodoIndex === -1) {
      alert(`No todo with id ${todoId} found!`);
      return;
    }
  
    todos.splice(foundTodoIndex, 1);

    populateTodos(todos, '#todos');
};
export const toggle = (ev) => {
    const todoId = +$(ev.target).attr('data-check-id');
    const foundTodo = todos.find(todo => todo.id === todoId);

    if (!foundTodo) {
        alert(`No todo with id ${todoId} found!`);
        return;
      }
    
      foundTodo.isDone = !foundTodo.isDone;
    
      populateTodos(todos, CONTAINER);
};
export const todoAdd = (ev) => {
    const $todotext = $('#todo-text');
    const $tododate = $('#todo-date');

    if (!$($todotext).val()) {
        alert('Please enter a valid text.');
        return;
    }
    if (!$($tododate).val()) {
        alert('Invalid date!');
        return;
    }
    statics.id++;
    const newToDo = {
        id: statics.id,
        name: $todotext.val(),
        due: $tododate.val(),
        isDone: false
    };

    todos.push(newToDo);

    populateTodos(todos, '#todos');

    $todotext.val('');
    $tododate.val('');
}





