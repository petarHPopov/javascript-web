const changeElement = () => {
  const header = document.getElementById('header');

  header.style.backgroundColor = 'red';
  header.style.color = 'white';
};

const resetElement = () => {
  const header = document.getElementById('header');

  header.style.backgroundColor = 'white';
  header.style.color = 'black';
};
