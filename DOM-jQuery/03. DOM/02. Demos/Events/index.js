const addTodo = () => {
  const input = document.getElementById('text-input');
  const list = document.getElementById('todos-list');
  const todo = document.createElement('li');

  todo.innerText = input.value;

  // Add todo to the list
  list.appendChild(todo);

  // Clear input
  input.value = '';
};


(() => {
  const btn = document.getElementById('btn');
  btn.addEventListener('click', (event) => {
    console.log(event);
    addTodo();
  });
})();
