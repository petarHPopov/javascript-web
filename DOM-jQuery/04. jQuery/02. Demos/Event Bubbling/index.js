const addTodo = (text) => {
  alert(text);
};

(() => {
  $('#btn').on('click', function(event) {
    // You could stop the propagation of the event up the chain
    // event.stopPropagation();

    // This refers to the object on which the event is attached and fired from
    // !!!! It won't work with Arrow function as it doesn't have its own this
    addTodo(this.id);
  });

  $('#inner').on('click', function(event) {
    addTodo(this.id);
  });

  $('#outer').on('click', function(event) {
    addTodo(this.id);
  });
})();
