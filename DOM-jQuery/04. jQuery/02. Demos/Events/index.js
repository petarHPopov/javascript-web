const addTodo = () => {
  const $input = $('#text-input');
  const $list = $('#todos-list');
  const $todo = $(`<li>${$input.val()}</li>`);

  // Add todo to the list
  $list.append($todo);

  // Clear input
  $input.val('');
};

(() => {
  $('#btn').on('click', (event) => {
    console.log(event);
    addTodo();
  });
})();
