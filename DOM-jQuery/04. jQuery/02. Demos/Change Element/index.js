const changeElement = () => {
  const $header = $('#header');

  $header.css('background-color', 'red');
  $header.css('color', 'white');
};

const resetElement = () => {
  const $header = $('#header');

  $header.css('background-color', 'white');
  $header.css('color', 'black');
};
