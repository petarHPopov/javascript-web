const addTodo = () => {
  const $input = $('#text-input');
  const $list = $('#todos-list');
  const $todo = $(`<li>${$input.val()}</li>`);


  if (!$input.val()) {
    alert('Input is empty!')
  } else {
    // Add todo to the list
    $list.append($todo);

    // Clear input
    $input.val('');
  }
};
