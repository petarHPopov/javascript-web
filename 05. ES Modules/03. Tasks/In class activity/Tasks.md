<img src="https://webassets.telerikacademy.com/images/default-source/logos/telerik-academy.svg)" alt="logo" width="300px" style="margin-top: 20px;"/>

## jQuery - Tasks

Use the **Todo app** which you've already created or the Template provided

!['todo-app'](./imgs/todo-app-animation.gif)

1. Make the application modular by using ES Modules
    - What is needed in order to make it work in the browser?
    - Some of the files are ready to use, but some are your responsibility
    - Be careful because you need the extension when importing modules in the browser

    This won't work
    
    ```js
    import {todos} from './todos'; 
    ```

    This will work

    ```js
    import {todos} from './todos.js';
    ``` 
