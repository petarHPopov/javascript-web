import { todos } from './database.js';
import { todoDelete, todoToggle, todoAdd, populateTodos } from './views.js';
import { deleteTodo, addTodo, toggleTodo } from './events.js';
import { CONTAINER } from './common.js'


// document.onready
$(() => {

  // Initial populate
  populateTodos(todos, CONTAINER);

  // Attach events here
  addTodo(todoAdd);
  
  toggleTodo(todoToggle);
  
  deleteTodo(todoDelete);

});
