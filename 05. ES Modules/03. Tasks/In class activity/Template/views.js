import { todos } from './database.js';
import { statics } from './common.js';

export const populateTodos = (todos, container) => {
  const $div = $(container);
  $div.empty();
  todos.forEach((todo) => todoView(todo, container));
};

const todoView = (todo, container) => {
  const $div = $(container);
  $div.append(`
  <div>
    <span>${todo.name} - ${moment(todo.due).format('MMMM Do YYYY')}</span>
    <input data-check-id="${todo.id}" type="checkbox" ${todo.isDone ? 'checked' : ''}/>
    <button data-id="${todo.id}">X</button>
  </div>
  `);
};

export const todoRemove = (ev) => {
  const todoId = +$(ev.target).attr('data-id');
  const foundTodoIndex = todos.findIndex(todo => todo.id === todoId);

  if(foundTodoIndex !== -1) {
    todos.splice(foundTodoIndex, 1);
  }

  populateTodos(todos, '#todos');
};

export const todoAdd = (ev) => {
  const $text = $('#todo-text')
  const $date = $('#todo-date')

  if ($($date).val()) {
    statics.id++;
    todos.push({ id: statics.id, name: $text.val(), due: $date.val(), isDone: false });

    populateTodos(todos, '#todos');
    $text.val('');
    $date.val('');
  } else {
    alert('Invalid date');
  }
};

export const todoToggle = (ev) => {
  const $target = $(ev.target);

  todos = todos.map((todo) => {
    if (todo.id === +$target.attr('data-check-id')) {
      todo.isDone = !$target.attr('checked');
    }

    return todo;
  });

  populateTodos(todos, '#todos');
};

