let todos = [
  { id: 0, name: 'Buy milk', due: '2019-05-30', isDone: true },
  { id: 1, name: 'Buy beer', due: '2019-06-03', isDone: false },
  { id: 2, name: 'Read "The Hitchhiker\'s Guide to the Galaxy" book', due: '2019-06-14', isDone: false },
];

export { todos }
