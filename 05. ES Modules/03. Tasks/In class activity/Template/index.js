import { addTodo, deleteTodo, toggleTodo } from './events.js';
import { todoRemove, populateTodos, todoAdd, todoToggle } from './views.js';
import { todos } from './database.js';



$(() => {
  // Initial populate
  populateTodos(todos, '#todos');

  // Delete todo
  deleteTodo(todoRemove);

  addTodo(todoAdd);

  toggleTodo(todoToggle);
});
