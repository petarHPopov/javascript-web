// Calling foo from the module
foo(); // foo called from module

// Accidentally overwrite foo
var foo = 42;

foo(); // TypeError: foo is not a function
