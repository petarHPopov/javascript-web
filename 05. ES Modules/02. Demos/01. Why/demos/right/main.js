// Calling foo from the module
foo(); // foo called from module

// const could not be overwritten
var foo = 42; // SyntaxError: Identifier 'foo' has already been declared

foo();
