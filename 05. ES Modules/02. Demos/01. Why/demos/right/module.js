// Always use const for functions
// In general try to always use const
const foo = function () {
  console.log('foo called from module');
};
